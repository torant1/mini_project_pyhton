import os

os.chdir(r'C:\Users\Serge\Documents\serge\Projet\exo2')
import string
def exo_fichier2(file):
    '''
    Program that reads a file,breaks line into words,strips white space and punctuation
    from words,and converts them to lowercase.
    Then modify the program to count the total number of words in the book, and the number of times each word is used.
Print the number of different words used in the book. Compare different books by different authors,
written in different eras. Which author uses the most extensive vocabulary?  

    '''
#la variable lecture servira a faire des test sur le fichier 
    lecture = False
    nombre_de_mots = 0
    liste_conteneur = []
    mot_tempon = ''
    mots_semblables=dict()
    affichage =()
    mots_espacers = []
    file = open(file)
    for ligne in file.readlines():
          #si le 
        if ligne.find("*** START OF THIS PROJECT GUTENBERG EBOOK") != -1:
            lecture = True
        elif ligne.find("*** END OF THIS PROJECT GUTENBERG EBOOK") != -1: #reached end of ebook, so stop adding lines
            lecture= False       
        elif lecture and ligne.find("*** START OF THIS PROJECT GUTENBERG EBOOK") == -1 and len(ligne) > 1:
            espaces_vides = ligne.strip() 
            #split each line into a list of individual strings
            liste_mots = espaces_vides.split()  
            for mot in liste_mots:
                #retirer les ponctuations et convertit certains lettres en minuscules
                mot = mot.translate(string.maketrans("",""), string.punctuation).lower() 
                 #concatener les chaines pour former une chaine contenant tous les mots
                mots_espacers = mots_espacers + [mot]
                lignes=" ".join(mots_espacers)
                #incrementation du nombre de mots deja lus
                nombre_de_mots = nombre_de_mots + 1
                cles = mots_semblables.keys()
                if  mot in cles:
                    mots_semblables[mot]=mots_semblables[mot]+1
                else :
                     mots_semblables[mot]=1
                     
    for cle,valeur in mots_semblables.items():
        affichage(cle,valeur)
    
    mot_differents=len(mots_semblables)
    return mot_differents, nombre_de_mots,affichage
                 
            
    

if __name__ == '__main__':
    p = exo_fichier2('fichier2.txt')
    print "exo_fichier2: {0}\n".format(p)
    os.system("pause")
