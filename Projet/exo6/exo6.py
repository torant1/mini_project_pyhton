# -*- coding: utf-8 -*-
"""
program that uses set subtraction to ﬁnd words in the book that are not in the word list. 
"""
import os
import sys, string

os.chdir(r'C:\Users\Serge\Documents\serge\Projet\exo6')
from collections import OrderedDict

def lineProcess(stringin, setin):
    """pour une ligne donnee,rajoute au dictionnaire des noms et son indice associe"""
    ligne = stringin.strip()
    words = ligne.split()
    for mot in words:
        mot = mot.translate(string.maketrans("",""), string.punctuation + string.digits).lower()
        setin.add(mot)
    return setin

def fileProcess(fin, processFunc):
    """retourne un ensemble de mots disctints"""
    s = set()
    try:
        mon_fichier = open(fin, 'r')
        for ligne in mon_fichier.readlines():
            processFunc(ligne, s)
        return s
    except:
        e = sys.exc_info()
        print e
    finally:
        mon_fichier.close()
        
if __name__ == '__main__':
    fichier_livre = 'fichier1.txt'
    fichier_mots = 'word.txt'
    mots_du_livre = fileProcess(fichier_livre, lineProcess)
    ensemble_mots = fileProcess(fichier_mots, lineProcess)

    mots_diffrents = mots_du_livre.difference(ensemble_mots)
    print mots_diffrents
    os.system("pause")
