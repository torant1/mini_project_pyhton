import os

os.chdir(r'C:\Users\Serge\Documents\serge\Projet\exo3')
import string
def exo_fichier3(file):
    '''
    Program that reads a file,breaks line into words,strips white space and punctuation
    from words,and converts them to lowercase.
    Then modify the program to count the total number of words in the book, and the number of times each word is used.
Print the number of different words used in the book. Compare different books by different authors,
written in different eras. Which author uses the most extensive vocabulary?  

    '''
  
    lecture = False
    nombre_de_mots = 0
    liste_conteneur = []
    mot_tempon = ''
    mots_semblables=dict()
    affichage =[]
    mots_espacers = []
    
    file = open(file)
    for ligne in file.readlines():
          
        if ligne.find("*** START OF THIS PROJECT GUTENBERG EBOOK") != -1: #test si le fichier ne se trouve pas au commencement du fichier
            lecture = True   
        elif ligne.find("*** END OF THIS PROJECT GUTENBERG EBOOK") != -1: #si le fichier ne se trouve a la fin du fichier
            lecture= False    
        elif lecture and ligne.find("*** START OF THIS PROJECT GUTENBERG EBOOK") == -1 and len(ligne) > 1: #si le fichier se trouve au commencement         
            espaces_vides = ligne.strip()  #retirer les espaces a gauche et a droite   
            liste_mots = espaces_vides.split()   #scinder chaque ligne et met le contenu dans une liste
            for mot in liste_mots:
                mot = mot.translate(string.maketrans("",""), string.punctuation).lower()  #retirer les ponctuations et convertit certains lettres en minuscules 
                mots_espacers = mots_espacers + [mot]  #concatener les chaines pour former une chaine contenant tous les mots
                lignes=" ".join(mots_espacers)
                nombre_de_mots = nombre_de_mots + 1 #incrementation du nombre de mots deja lus
                cles = mots_semblables.keys()
                if  mot in cles:
                    mots_semblables[mot]=mots_semblables[mot]+1
                else :
                     mots_semblables[mot]=1
                  
    mot_differents=len(mots_semblables)
    inventaire_inverse=[(cles,mot) for mot,cles in mots_semblables.items() ] #inversion de la liste pour touver les indices en premier
    inventaire=[(mot,cles) for cles,mot in sorted(inventaire_inverse,reverse=True)] #reorganisation de la liste dans l'ordre
    for i in range(20): #boucle pour trouver les 20 elements les plus reccurents et les ajoutes a la liste affichage
       affichage.append(inventaire[i])#
      
    return affichage
                 
            
    

if __name__ == '__main__':
    p = exo_fichier3('fichier2.txt')
    print "les 20 mots les plus reccurents: {0}\n".format(p)
    os.system("pause")
