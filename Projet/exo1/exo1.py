import os
os.chdir(r'C:\Users\Serge\Documents\serge\Projet\exo1')


import string

def exo_fichier1(file):
    '''
    Program that reads a file,breaks line into words,strips white space and punctuation
    from words,and converts them to lowercase
    '''

    file = open(file)
    #variables pour stocker les mots qu'on va enlever les espaces a gauche et a droite
    mots_espacers = []

    for ligne in file.readlines():
        #enlever les espaces a gauche et a droite
        espaces_vides = ligne.strip() 
        #split each line into a list of individual strings
        liste_mots = espaces_vides.split()  
        for mot in liste_mots:
            #retirer les ponctuations et convertit certains lettres en minuscules
            mot = mot.translate(string.maketrans("",""), string.punctuation).lower() 
             #concatener les chaines pour former une chaine contenant tous les mots
            mots_espacers = mots_espacers + [mot]
            lignes=" ".join(mots_espacers)
             #retourne unechaine contenant tous les mots
    return lignes 
    
if __name__ == '__main__':
    p = exo_fichier1('fichier1.txt')
    print "exo_fichier1: {0}\n".format(p)
